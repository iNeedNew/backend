CREATE TABLE roles(
id INT AUTO_INCREMENT PRIMARY KEY,
named VARCHAR(32)
)


CREATE TABLE users (
id INT AUTO_INCREMENT PRIMARY KEY,
role_id INT,
username VARCHAR(16),
email VARCHAR(32),
password_hash VARCHAR(128),
first_name VARCHAR(32),
last_name VARCHAR(32),
location VARCHAR(64),
bod DATE,
description VARCHAR(512),
create_account DATE,
is_blocked INTEGER
)


CREATE TABLE categories (
id INT AUTO_INCREMENT PRIMARY KEY,
named VARCHAR(32)
)


CREATE TABLE types (
id INT AUTO_INCREMENT PRIMARY KEY,
named VARCHAR(32)
)


CREATE TABLE publications(
id INT AUTO_INCREMENT PRIMARY KEY,
user_id INT,
category_id INT,
type_id INT,
title VARCHAR(32),
url VARCHAR(128),
filename VARCHAR(32),
filename_hash VARCHAR(128),
published DATE,
is_blocked INT
)

CREATE TABLE likes_dislikes(
user_id INT,
content_id INT,
datetime DATE
)


CREATE TABLE comments(
user_id INT,
content_id INT,
parent_id INT,
text VARCHAR(256),
datetime DATE,
is_blocked INT
)