from flask import jsonify

from app import connect_db
from app.api import api
from app.model import Service

VERSION = 'v1'


@api.route(f'/{VERSION}/users/<int:user_id>', methods=['GET'])
def get_user(user_id):
    service = Service(connect_db())
    answer = service.get_user(user_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/users/<int:user_id>/all_publications', methods=['GET'])
def get_publications_user(user_id):
    service = Service(connect_db())
    answer = service.get_publications_user(user_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/users/<int:user_id>/all_publications/top', methods=['GET'])
def get_top_publications_user(user_id):
    service = Service(connect_db())
    answer = service.get_top_publications_user(user_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/users/<int:user_id>/all_publications/news', methods=['GET'])
def get_new_publications_user(user_id):
    service = Service(connect_db())
    answer = service.get_new_publications_user(user_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/users/<int:user_id>/<type_publications>', methods=['GET'])
def get_type_publications_user(user_id, type_publications):
    service = Service(connect_db())
    type_id = service.get_type_id(type_publications)
    answer = service.get_type_publications_user(user_id, type_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/users/<int:user_id>/<type_publications>/top', methods=['GET'])
def get_top_type_publications_user(user_id, type_publications):
    service = Service(connect_db())
    type_id = service.get_type_id(type_publications)
    answer = service.get_top_type_publications_user(user_id, type_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/users/<int:user_id>/<type_publications>/news', methods=['GET'])
def get_new_type_publications_user(user_id, type_publications):
    service = Service(connect_db())
    type_id = service.get_type_id(type_publications)
    answer = service.get_new_type_publications_user(user_id, type_id)
    return jsonify(answer)


# ----- user-change -----
@api.route(f'/{VERSION}/users/<int:user_id>/comments', methods=['GET'])
def get_comment_user(user_id):
    return 'Комментарии конкретног юзера'


@api.route(f'/{VERSION}/users/<int:user_id>/comments/top', methods=['GET'])
def get_top_comment_user(user_id):
    return 'Лучшие комментарии конкретног юзера'


@api.route(f'/{VERSION}/users/<int:user_id>/comments/news', methods=['GET'])
def get_new_comment_user(user_id):
    return 'Последние комментарии конкретног юзера'


# ----- publication -----
@api.route(f'/{VERSION}/all_publications', methods=['GET'])
def get_publications():
    service = Service(connect_db())
    answer = service.get_publications()
    return jsonify(answer)


@api.route(f'/{VERSION}/all_publications/top', methods=['GET'])
def get_top_publications():
    service = Service(connect_db())
    answer = service.get_top_publications()
    return jsonify(answer)


@api.route(f'/{VERSION}/all_publications/news', methods=['GET'])
def get_new_publications():
    service = Service(connect_db())
    answer = service.get_new_publications()
    return jsonify(answer)


@api.route(f'/{VERSION}/<string:type_publications>', methods=['GET'])
def get_type_publications(type_publications):
    service = Service(connect_db())
    type_id = service.get_type_id(type_publications)
    answer = service.get_type_publications(type_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/<string:type_publications>/top', methods=['GET'])
def get_top_type_publications(type_publications):
    service = Service(connect_db())
    type_id = service.get_type_id(type_publications)
    answer = service.get_top_type_publications(type_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/<string:type_publications>/news', methods=['GET'])
def get_new_type_publications(type_publications):
    service = Service(connect_db())
    type_id = service.get_type_id(type_publications)
    answer = service.get_new_type_publications(type_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/publications/<int:publication_id>', methods=['GET'])
def get_publication(publication_id):
    service = Service(connect_db())
    answer = service.get_publication(publication_id)
    return jsonify(answer)


@api.route(f'/{VERSION}/publications/<int:publication_id>/comments', methods=['GET'])
def get_comments_publication():
    return 'Комментарии конкретного контента'
