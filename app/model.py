from datetime import datetime

from pymysql import MySQLError
from werkzeug.security import generate_password_hash


class Service:
    """Класс для работы с данными в базе данных"""

    def __init__(self, db):
        """Инциализация коннектора (mysql)"""
        self.__db = db
        self.__cur = db.cursor()

    def __get(self, sql: str, parameters: tuple = None) -> list:
        """Взять данные из базы данных"""
        try:
            if parameters is None:
                self.__cur.execute(sql)
                return self.__cur.fetchall()

            self.__cur.execute(sql, parameters)
            return self.__cur.fetchall()

        except MySQLError as error:
            print('Error', error)
            return []

    def get_user(self,
                 user_id: int) -> list:
        """Взять конкретного пользователя по id"""

        sql = """
        SELECT * FROM users WHERE users.id=(%s)
        """
        parameters = (user_id,)
        return self.__get(sql, parameters)

    def get_publication(self, publication_id):
        """Взять конкретную публикациб по id"""

        sql = """
        SELECT publications.* FROM publications
        WHERE publications.id = (%s)
        """
        parameters = (publication_id,)
        return self.__get(sql, parameters)

    def get_publications_user(self, user_id):
        """Взять публикации конкретного пользователя по его id"""

        sql = """
        SELECT * FROM publications WHERE publications.user_id=(%s)
        """
        parameters = (user_id,)
        return self.__get(sql, parameters)

    def get_top_publications_user(self, user_id):
        """Взять лучшие публикации конкретного пользователя по его id"""

        sql = """
        SELECT publications.*, COUNT(likes_dislikes.content_id) as like_count FROM publications
        JOIN likes_dislikes ON likes_dislikes.content_id = publications.id
        WHERE likes_dislikes.action = 1 AND publications.user_id = (%s)
        GROUP BY likes_dislikes.content_id
        """
        parameters = (user_id,)
        return self.__get(sql, parameters)

    def get_new_publications_user(self, user_id):
        """Взять последние публикации конкретного пользователя по его id"""

        sql = """
        SELECT publications.* FROM publications
        WHERE publications.user_id = (%s)
        ORDER BY publications.published DESC
        """
        parameters = (user_id,)
        return self.__get(sql, parameters)

    def get_type_publications_user(self, user_id, type_id):
        """Взять определенный тип контента по type_id конкретного пользователя по его id"""

        sql = """
        SELECT publications.* FROM publications 
        WHERE publications.user_id=(%s) AND publications.type_id = (%s)
        """
        parameters = (user_id, type_id)
        return self.__get(sql, parameters)

    def get_top_type_publications_user(self, user_id, type_id):
        """Взять луший определенный тип контента по type_id конкретного пользователя по его id"""

        sql = """
        SELECT publications.*, COUNT(likes_dislikes.content_id) as like_count FROM publications
        JOIN likes_dislikes ON likes_dislikes.content_id = publications.id
        WHERE likes_dislikes.action = 1 AND publications.user_id = (%s) AND publications.type_id = (%s)
        GROUP BY likes_dislikes.content_id
        """
        parameters = (user_id, type_id)
        return self.__get(sql, parameters)

    def get_new_type_publications_user(self, user_id, type_id):
        """Взять последний определенный тип контента по type_id конкретного пользователя по его id"""

        sql = """
        SELECT publications.* FROM publications
        WHERE publications.user_id = (%s) AND publications.type_id = (%s)
        ORDER BY published DESC
        """
        parameters = (user_id, type_id)
        return self.__get(sql, parameters)

    def get_publications(self):
        """Взять публикации"""

        sql = """
        SELECT publications.* FROM publications"
        """
        return self.__get(sql)

    def get_top_publications(self):
        """Взять лучшие публикации"""

        sql = """
        SELECT publications.*, COUNT(likes_dislikes.content_id) as like_count FROM publications
        JOIN likes_dislikes ON likes_dislikes.content_id = publications.id
        WHERE likes_dislikes.action = 1
        GROUP BY likes_dislikes.content_id
        """
        return self.__get(sql)

    def get_new_publications(self):
        """Взять последние публикации"""

        sql = """
        SELECT publications.* FROM publications
        ORDER BY publications.published DESC
        return self.__get(sql)
        """
        return self.__get(sql)

    def get_type_publications(self, type_id):
        """Взять определенный тип контента по type_id"""

        sql = """
        SELECT publications.* FROM publications
        WHERE publications.type_id = (%s)
        """
        parameters = (type_id,)
        return self.__get(sql, parameters)

    def get_top_type_publications(self, type_id):
        """Взять лучший определенный тип контента по type_id"""

        sql = """
        SELECT publications.*, COUNT(likes_dislikes.content_id) as like_count FROM publications
        JOIN likes_dislikes ON likes_dislikes.content_id = publications.id
        WHERE likes_dislikes.action = 1 AND publications.type_id = (%s)
        GROUP BY likes_dislikes.content_id
        """
        parameters = (type_id,)
        return self.__get(sql, parameters)

    def get_new_type_publications(self, type_id):
        """Взять последний определенный тип контента по type_id"""

        sql = """
        SELECT publications.* FROM publications
        WHERE publications.type_id = (%s)
        ORDER BY publications.published DESC
        """
        parameters = (type_id,)
        return self.__get(sql, parameters)

    def get_category_id(self, category_name: str):
        """Взять id категории по ее названию"""

        sql = """
        SELECT categories.id FROM categories WHERE LOWER(categories.named) = (%s) LIMIT 1
        """
        parameters = (category_name,)
        return self.__get(sql, parameters)[0]['id']

    def get_type_id(self, type_name: str):
        """Взять id типа по его названию"""

        sql = """
        SELECT types.id FROM types WHERE LOWER(types.named) = (%s) LIMIT 1
        """
        parameters = (type_name,)
        return self.__get(sql, parameters)[0]['id']

    def __add(self, sql: str, parameters: tuple):
        """Добавить данные в БД"""

        try:
            self.__cur.execute(sql, parameters)
            self.__db.commit()
        except MySQLError as error:
            print(error)

    def add_user(self,
                 username: str = None,
                 email: str = None,
                 password: str = None):
        """Добавить пользователя в БД"""

        sql = """
        INSERT INTO users VALUES(Null, Null, %s, %s, %s, Null, Null, Null, Null, Null, Null, Null)
        """
        password_hash = generate_password_hash(generate_password_hash(password))
        parameters = (username, email, password_hash)
        self.__add(sql, parameters)

    def add_publications(self,
                         user_id: int = None,
                         category_id: int = None,
                         type_id: int = None,
                         title: str = None,
                         url: str = None,
                         filename: str = None
                         ):
        """Добавить публикацию в БД"""

        sql = """
        INSERT INTO publications VALUES(Null, %s, %s, %s, %s, %s, %s, NULL, NULL, %s)
        """
        published = datetime.now()
        parameters = (user_id, category_id, type_id, title, url, filename, published)
        self.__add(sql, parameters)

    def add_category(self, category_name):
        """Добавить новую категорию в БД"""

        sql = """
        INSERT INTO categories VALUES(Null, %s)
        """
        lower_category_name = category_name.lower()
        parameters = (lower_category_name,)
        return self.__add(sql, parameters)

    def __check(self, sql, parameters):
        """Проверить на совпадение в БД"""
        try:
            self.__cur.execute(sql, parameters)
            return bool(self.__cur.fetchone())
        except MySQLError as error:
            print(error)
            return -1

    def check_category(self, category_name: str):
        """Проверить на схожесть категории"""

        sql = """
        SELECT * FROM categories WHERE LOWER(categories.named)=(%s) LIMIT 1
        """
        parameters = (category_name,)
        return self.__check(sql, parameters)
