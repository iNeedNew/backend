from flask import Flask
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor

from config import config_dict

mysql = MySQL(cursorclass=DictCursor)


def connect_db():
    return mysql.connect()


def create_app(config_name):
    """Фабричный шаблон создания приложения. Запуская его с разными конфигами"""

    app = Flask(__name__)
    app.config.from_object(config_dict[config_name])

    mysql.init_app(app)

    from app.api import api
    app.register_blueprint(api)

    return app
