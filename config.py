class Configuration:
    pass


class Development(Configuration):
    # конфигурация базы данных
    MYSQL_DATABASE_HOST = 'localhost'
    MYSQL_DATABASE_PORT = 3306
    MYSQL_DATABASE_USER = 'backend'
    MYSQL_DATABASE_PASSWORD = 'BaC@AnS1!3QaDfgHhh3322@'
    MYSQL_DATABASE_DB = 'dev_backend_db'

    DEBUG = True


config_dict = {
    'development': Development,
}
