from flask_script import Manager, Shell

from app import create_app, connect_db
from app.model import Service

app = create_app('development')

manage = Manager(app)

db = Service(connect_db())

@app.shell_context_processor
def make_shell_context():
    return dict(
        app=app,
        connect_db=connect_db,
        db=db,
    )


manage.add_command("shell", Shell(make_context=make_shell_context))

if __name__ == '__main__':
    manage.run()
